package com.example.zafar.nearbyfriends.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.zafar.nearbyfriends.helpers.CommonMethods;
import com.example.zafar.nearbyfriends.helpers.Constants;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class BaseActivity extends AppCompatActivity {
    public static LatLng userCurrentLocation;
    public static void didUpdateUserLocation(Context context) {

        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient ( context );

        client.getLastLocation ().addOnSuccessListener ( (Activity) context , location -> {
            if (location != null) {
                LatLng latLng = new LatLng ( location.getLatitude () , location.getLongitude () );
                userCurrentLocation = latLng;
                ((SignUpActivity)context).didUpdateUserLocation ();
            }
            else{
                CommonMethods.showAlertWithTitleAndMessage ( context, Constants.ERROR, Constants.LOCATION_NOT_FOUND_ERROR_MESSAGE );
            }
        } );
    }
    public static void requestLocationPermission(Context context){
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{ACCESS_FINE_LOCATION}, Constants.PERMISSION_REQUEST_CODE);
        }
        else {
            didUpdateUserLocation( context );
        }
    }

}
