package com.example.zafar.nearbyfriends.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import com.example.zafar.nearbyfriends.fragments.ContactsFragment;
import com.example.zafar.nearbyfriends.fragments.SearchFragment;
import com.example.zafar.nearbyfriends.R;
import com.example.zafar.nearbyfriends.fragments.SettingsFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_home);
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationID );
        bottomNavigationView.setOnNavigationItemSelectedListener
                ( item -> {
                    Fragment selectedFragment = null;
                    switch (item.getItemId()) {
                        case R.id.navigation_contacts:
                            selectedFragment = ContactsFragment.newInstance();
                            break;
                        case R.id.navigation_search:
                            selectedFragment = SearchFragment.newInstance();
                            break;
                        case R.id.navigation_setting:
                            selectedFragment = SettingsFragment.newInstance();
                            break;
                    }
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frameLayoutID , selectedFragment);
                    transaction.commit();
                    return true;
                } );
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayoutID , ContactsFragment.newInstance());
        transaction.commit();
    }
}