package com.example.zafar.nearbyfriends.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.zafar.nearbyfriends.R;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_login );
    }
    public void goToSignUpClicked(View view){
        Intent intent = new Intent ( this, SignUpActivity.class );
        startActivity ( intent );
    }
}
