package com.example.zafar.nearbyfriends.activities;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.zafar.nearbyfriends.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Objects;

public class MapActivity extends BaseActivity implements OnMapReadyCallback {
    private String[] nearByFriendNames = {"H. Hunter", "John Mick", "Jack Holder", "Jeo Hike", "Rohan Ali"};
    private double[] nearByFriendsDistance = {1000, 209, 5000, 4580, 2890};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_map );
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager ()
                .findFragmentById ( R.id.map );
        Objects.requireNonNull ( mapFragment ).getMapAsync ( this );
        ListView nearByFriendsList = findViewById ( R.id.listNearFriends );
        nearByFriendsList.setAdapter  ( new CustomAdapter () );
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.addMarker ( new MarkerOptions ().position ( userCurrentLocation ).title ( "Your Current Location" ) );
        googleMap.moveCamera ( CameraUpdateFactory.newLatLng ( userCurrentLocation ) );
    }
    public class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return nearByFriendNames.length;
        }
        @Override
        public Object getItem(int position) {
            return null;
        }
        @Override
        public long getItemId(int position) {
            return 0;
        }
        @Override
        public View getView(int position , View convertView , ViewGroup parent) {
            if ( position == 0 ){
                convertView = getLayoutInflater ().inflate ( R.layout.fragment_near_friend_list_top , null );
                convertView.setEnabled ( false );
                convertView.setOnClickListener (null);
            }
            else {
                convertView = getLayoutInflater ().inflate ( R.layout.fragment_list_near_friend_item , null );
                TextView nearByFriendName = convertView.findViewById ( R.id.nameNearByFriendID );
                TextView nearByFriendDistance = convertView.findViewById ( R.id.distanceNearByFriendID );
                nearByFriendName.setText ( nearByFriendNames [position] );
                if( 1000 > nearByFriendsDistance[position] ){
                    nearByFriendDistance.setText ( (int) nearByFriendsDistance[position] +"m" );
                }else {
                    nearByFriendDistance.setText ( ( nearByFriendsDistance[position] ) / 1000 + "km" );
                }
            }
            return convertView;
        }
    }
}