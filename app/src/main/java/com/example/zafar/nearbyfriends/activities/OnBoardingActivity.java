package com.example.zafar.nearbyfriends.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import com.example.zafar.nearbyfriends.R;

public class OnBoardingActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_on_boarding );
    }
    public void goButtonClicked(View view){
        Intent intent = new Intent ( this , LoginActivity.class);
        startActivity(intent);
    }
}
