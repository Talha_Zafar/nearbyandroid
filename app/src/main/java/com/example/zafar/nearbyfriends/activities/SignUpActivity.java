package com.example.zafar.nearbyfriends.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;

import com.example.zafar.nearbyfriends.R;
import com.example.zafar.nearbyfriends.helpers.CommonMethods;
import com.example.zafar.nearbyfriends.helpers.Constants;
import com.example.zafar.nearbyfriends.interfaces.UserCurrentLocationInterface;
import com.example.zafar.nearbyfriends.models.UserModel;
import com.example.zafar.nearbyfriends.utils.PhoneNumberUtils;
import com.example.zafar.nearbyfriends.utils.TextFieldUtils;

import java.io.IOException;
public class SignUpActivity extends BaseActivity implements UserCurrentLocationInterface {

    private TextFieldUtils nameEditText, phoneNumberEditText, passwordEditText;
    private AppCompatCheckBox verifyPhoneNumberCheckBox;
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == Constants.PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                BaseActivity.didUpdateUserLocation ( this );
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_sign_up);
        initializeViews();

        phoneNumberFormatting ();
        requestLocationPermission (this);
    }
    public void initializeViews(){
        nameEditText = findViewById ( R.id.nameEditTextID );
        phoneNumberEditText = findViewById ( R.id.phoneNumberSignUpEditTextID );
        passwordEditText = findViewById ( R.id.passwordSignUpEditTextID );
        //verifyPhoneNumberCheckBox = findViewById ( R.id.checkBoxID );
    }
    @Override
    public void didUpdateUserLocation() {
        CommonMethods.showAlertWithTitleAndMessage(this,Constants.USER_LOCATION ,
                "Lat: " + userCurrentLocation.latitude + " Long: " + userCurrentLocation.longitude);
    }
    public void signUpButtonClicked(View view){
        if(isValidFields ()){
            UserModel userModel = new UserModel ();
            userModel.addUser ( nameEditText.getText ().toString (),phoneNumberEditText.getText ().toString (),
                    passwordEditText.getText ().toString (), userCurrentLocation.latitude,userCurrentLocation.longitude,this);
            Intent intent = new Intent ( this, HomeActivity.class );
            startActivity ( intent );
        }
    }
    public void phoneNumberFormatting(){
        phoneNumberEditText.addTextChangedListener(new PhoneNumberFormattingTextWatcher ());
    }
    private boolean isValidFields() {
        if ( nameEditText.isEmpty () ||
                !(PhoneNumberUtils.isValidPhoneNumber ( phoneNumberEditText.getText ().toString () )) ||
                passwordEditText.isEmpty ()) {

            CommonMethods.showAlertWithTitleAndMessage ( this, Constants.ERROR, Constants.ENTER_DETAILS_CORRECTLY_MESSAGE );
            return false;
        }
        return  true;
    }
    public  void checkBoxChecked(View view) {
        verifyPhoneNumberCheckBox.setOnCheckedChangeListener( (buttonView , isChecked) -> {
            if ( isChecked && PhoneNumberUtils.isValidPhoneNumber ( phoneNumberEditText.getText ().toString () ))
            {
                try {
                    PhoneNumberUtils.sendSMSToUserPhoneNumber (phoneNumberEditText.getText ().toString ());
                    PhoneNumberUtils.verifyPhoneNumberThroughOTP ( phoneNumberEditText.getText ().toString (), this);
                } catch (IOException ignored) {

                }
            }

        } );
    }
}