package com.example.zafar.nearbyfriends.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zafar.nearbyfriends.R;

public class ContactsAdapter extends BaseAdapter {

    private int[] contactsImagesList;
    private String[] contactNames;
    private String[] lastMessagesList;
    private Context context;

    public ContactsAdapter(String[] contactNames, String[] lastMessageList, int[] contactsImagesList ,Context context) {
        this.contactNames = contactNames;
        this.lastMessagesList = lastMessageList;
        this.contactsImagesList = contactsImagesList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return contactNames.length;
    }

    @Override
    public Object getItem(int position) {return null;}

    @Override
    public long getItemId(int position) {return 0;}

    @Override
    public View getView(int position , View convertView , ViewGroup parent) {
        convertView = LayoutInflater.from ( context ).inflate ( R.layout.fragment_contacts_list_view_item , null );
        ImageView contactImage = convertView.findViewById ( R.id.contactImageID );
        TextView contactName = convertView.findViewById ( R.id.contactNameID );
        TextView lastMessage = convertView.findViewById ( R.id.lastMessageID );

        contactImage.setImageResource ( contactsImagesList[position] );
        contactName.setText ( contactNames[position] );
        lastMessage.setText ( lastMessagesList[position] );
        return convertView;
    }
}