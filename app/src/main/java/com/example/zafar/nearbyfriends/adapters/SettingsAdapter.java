package com.example.zafar.nearbyfriends.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import android.widget.SeekBar;
import android.widget.TextView;
import android.view.View;
import android.view.ViewGroup;
import com.example.zafar.nearbyfriends.R;

public class SettingsAdapter extends BaseAdapter {

    private String[] settingsNames ;
    private Context context;
    
    public SettingsAdapter(String[] settingsNames , Context context) {
        this.settingsNames = new String[settingsNames.length];
        for (int i = 0;i<settingsNames.length ;i++){
            this.settingsNames[i] = settingsNames[i];
        }
        this.context = context;
    }

    @Override
    public int getCount() {
        return settingsNames.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position , View convertView , ViewGroup parent) {
        if ( position == 1 ){
            convertView = LayoutInflater.from ( context ).inflate ( R.layout.fragment_seekbar_settings_list_view_item , null );
            SeekBar seekBar = convertView.findViewById ( R.id.settingsSeekbarViewItemID );
            seekBar.getThumb ().setColorFilter ( convertView.getResources ().getColor ( R.color.themeColor), PorterDuff.Mode.SRC_IN );
            seekBar.getProgressDrawable ().setColorFilter ( convertView.getResources ().getColor ( R.color.themeColor ),  PorterDuff.Mode.SRC_IN );
            TextView seekbarValue =  convertView.findViewById (R.id.seekbarValueID);
            seekBar.setOnSeekBarChangeListener ( new SeekBar.OnSeekBarChangeListener () {

                @Override
                public void onProgressChanged(SeekBar seekBar , int progress , boolean fromUser) {
                    seekbarValue.setText ( progress + "" );
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) { }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) { }
            } );

            TextView textView = convertView.findViewById ( R.id.radiusSetTextID );
            textView.setText ( settingsNames[position] );
        }
        else {
            convertView = LayoutInflater.from ( context ).inflate ( R.layout.fragment_text_settings_list_view_item , null );
            TextView textView = convertView.findViewById ( R.id.settingsTextViewItemID);
            textView.setText ( settingsNames[position] );
            if(position == 3){
                textView.setTextColor ( Color.RED );
            }
        }
        return convertView;
    }
}