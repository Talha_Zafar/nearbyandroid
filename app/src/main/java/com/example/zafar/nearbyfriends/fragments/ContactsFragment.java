package com.example.zafar.nearbyfriends.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.example.zafar.nearbyfriends.R;
import com.example.zafar.nearbyfriends.adapters.ContactsAdapter;

public class ContactsFragment extends Fragment {

    public static ContactsFragment newInstance() {
        return new ContactsFragment ();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }
    private int[] contactsImagesList ={ R.drawable.img_1, R.drawable.img_2, R.drawable.img_3,
            R.drawable.img_4, R.drawable.img_5};
    private String[] contactsNames = {"H. Hunter", "John Mick", "Jack Holder", "Jeo Hike", "Rohan Ali"};
    private String[] lastMessagesList = {"Hope so you are good", "Good Bye!",
            "God Bless you", "Nice", "Will you come to uni ?"};
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate( R.layout.fragment_contacts , container, false);
        ListView listView = rootView.findViewById ( R.id.contactsListVIewID );
        ContactsAdapter contactsAdapter = new ContactsAdapter (contactsNames, lastMessagesList, contactsImagesList, this.getActivity ());
        listView.setAdapter ( contactsAdapter );
        return rootView;
    }
}
