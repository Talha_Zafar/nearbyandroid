package com.example.zafar.nearbyfriends.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;

import com.example.zafar.nearbyfriends.activities.MapActivity;
import com.example.zafar.nearbyfriends.R;

public class SearchFragment extends Fragment  {

    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment ();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate( R.layout.fragment_search , container, false);
        rootView.findViewById ( R.id.goToMapButtonID ).setOnClickListener ( v -> {
            Intent intent = new Intent ( this.getActivity () , MapActivity.class);
            startActivity(intent);
        });
        return rootView;
    }
}