package com.example.zafar.nearbyfriends.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.zafar.nearbyfriends.R;
import com.example.zafar.nearbyfriends.adapters.SettingsAdapter;
import com.example.zafar.nearbyfriends.helpers.Constants;

public class SettingsFragment extends Fragment {

    public static SettingsFragment newInstance() {
        return new SettingsFragment ();
    }
    private String[] settingsNames = new String[4];

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        settingsNames[0] = Constants.MY_PROFILE;
        settingsNames[1] = Constants.PREFERRED_RADIUS;
        settingsNames[2] = Constants.CHANGE_PASSWORD;
        settingsNames[3] = Constants.SIGN_OUT;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate( R.layout.fragment_settings, container, false);
        ListView listView = rootView.findViewById ( R.id.settingsListVIewID );
        SettingsAdapter settingsAdapter = new SettingsAdapter (settingsNames, this.getContext ());
        listView.setAdapter ( settingsAdapter );
        return rootView;
    }
}
