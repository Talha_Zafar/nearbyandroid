package com.example.zafar.nearbyfriends.helpers;

import android.content.Context;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;

public class CommonMethods {

    public static void showAlertWithTitleAndMessage(Context context, String title, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton(Constants.OK,(dialoginterface, i) -> { })
                .show();
    }
    public static void showAlertInputPinCode(Context context, String title, String message,
                                             EditText otpNUmberEditText) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(title)
                .setMessage(message)
                .setView ( otpNUmberEditText )
                .setPositiveButton(Constants.OK,(dialoginterface, i) -> { })
                .show();
    }
}
