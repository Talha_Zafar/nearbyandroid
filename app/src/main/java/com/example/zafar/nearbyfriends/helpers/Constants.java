package com.example.zafar.nearbyfriends.helpers;

public class Constants {
    public static final int PERMISSION_REQUEST_CODE = 1;

    public static final String OK = "OK";
    public static final String VERIFIED = "Verified";
    public static final String VERIFY = "Verify";
    public static final String API_KEY = "apikey";
    public static final String MESSAGE = "message";
    public static final String SENDER = "sender";
    public static final String NUMBERS = "numbers";

    public static final String USER_LOCATION = "User Location";
    public static final String MY_PROFILE = "My Profile";
    public static final String PREFERRED_RADIUS = "Preferred Radius";
    public static final String CHANGE_PASSWORD = "Change Password";
    public static final String SIGN_OUT = "Sign Out";
    public static final String ERROR = "Error";


    //Messages Segment
    public static final String VERIFYING_PHONE_NUMBER_MESSAGE = "Verifying Phone Number";
    public static final String WRONG_PIN_CODE_MESSAGE = "Worng Pin Code";
    public static final String ENTER_DETAILS_CORRECTLY_MESSAGE = "Please enter your details correctly";
    public static final String LOCATION_NOT_FOUND_ERROR_MESSAGE = "Location Not Found Error\nPlease try later.";
    public static final String VERIFICATION_CODE_MESSAGE = " is your NearByFriends verification code ";

    //URLS
    public static final String VERIFY_PHONE_NUMBER_URL = "https://api.txtlocal.com/send/?";

    //API KEYS
    public static final String SMS_API_KEY = "bi8vecD1Ogk-R04EmkAIRE6G7KOH2qZb5p2G6rLYXy";
}
