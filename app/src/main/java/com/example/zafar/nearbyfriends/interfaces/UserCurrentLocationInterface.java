package com.example.zafar.nearbyfriends.interfaces;

/**
 * Created by usmanshaukat on 15/07/2019.
 */

public interface UserCurrentLocationInterface {
    public void didUpdateUserLocation();
}
