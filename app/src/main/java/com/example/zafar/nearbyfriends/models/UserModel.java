package com.example.zafar.nearbyfriends.models;

import android.content.Context;

import com.firebase.client.Firebase;

public class UserModel {
    private static Firebase firebase;

    private String userName, userPhoneNumber, userPassword;
    private double latitude, longitude;

    public UserModel() {
    }

    public String getUserName(){return userName;}

    public String getUserPhoneNumber() {return userPhoneNumber;}
    public String getUserPassword() {return userPassword;}
    public double getLatitude() {return latitude;}
    public double getLongitude() {return longitude;}
    public void setUserName(String userName) { this.userName = userName;}
    public void setUserPhoneNumber(String userPhoneNumber) {this.userPhoneNumber = userPhoneNumber;}
    public void setUserPassword(String userPassword) {this.userPassword = userPassword;}
    public void setLatitude(double latitude) {this.latitude = latitude;}
    public void setLongitude(double longitude) {this.longitude = longitude;}


    public void addUser(String userName , String userPhoneNumber , String userPassword , double latitude , double longitude, Context context) {
        Firebase.setAndroidContext ( context );
        firebase = new Firebase ( "https://nearbyfriends.firebaseio.com/user" );
        String id  = firebase.push ().getKey ();
        firebase.child ( id ).child ( "name" ).setValue ( userName );
        firebase.child ( id ).child ( "phoneNumber" ).setValue ( userPhoneNumber );
        firebase.child ( id ).child ( "password" ).setValue ( userPassword );
        firebase.child ( id ).child ( "latitude" ).setValue ( latitude );
        firebase.child ( id ).child ( "longitude" ).setValue ( longitude );
    }
}
