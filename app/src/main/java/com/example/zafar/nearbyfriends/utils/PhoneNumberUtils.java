package com.example.zafar.nearbyfriends.utils;

import android.content.Context;
import android.widget.EditText;

import com.example.zafar.nearbyfriends.R;
import com.example.zafar.nearbyfriends.helpers.CommonMethods;
import com.example.zafar.nearbyfriends.helpers.Constants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

import static java.net.URLEncoder.encode;

public class PhoneNumberUtils {

    private static int randomNumber;

    public static void sendSMSToUserPhoneNumber(String phoneNumber) throws IOException {

        Random random = new Random ();
        randomNumber = random.nextInt (999999);
        String apiKey = Constants.API_KEY + "=" + encode(Constants.SMS_API_KEY, "UTF-8");
        String message = "&" + Constants.MESSAGE + "=" + encode( randomNumber + Constants.VERIFICATION_CODE_MESSAGE , "UTF-8");
        String sender = "&" + Constants.SENDER + "=" + encode( String.valueOf ( R.string.app_name ) , "UTF-8");
        String numbers = "&" + Constants.NUMBERS + "=" + encode(phoneNumber, "UTF-8");

        String data = Constants.VERIFY_PHONE_NUMBER_URL + apiKey + numbers + message + sender;
        URL url = new URL(data);
        URLConnection conn = url.openConnection();
        conn.setDoOutput(true);

        BufferedReader read = new BufferedReader(new InputStreamReader (conn.getInputStream()));
        String line;
        String sResult="";
        while ((line = read.readLine()) != null) {
            sResult=sResult+line+" ";
        }
        read.close();
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        if (phoneNumber.equals ( "" ) || phoneNumber.length () > 15 ||
                phoneNumber.length () < 8) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher ( phoneNumber ).matches ();
        }
    }
    public static void verifyPhoneNumberThroughOTP(String phoneNumber, Context context) {
        EditText otpNUmberEditText = new EditText ( context );
        CommonMethods.showAlertInputPinCode (context, Constants.VERIFYING_PHONE_NUMBER_MESSAGE , Constants.VERIFY, otpNUmberEditText );
        if (randomNumber == Integer.valueOf ( otpNUmberEditText.getText ().toString () )){
            CommonMethods.showAlertWithTitleAndMessage (context, Constants.VERIFYING_PHONE_NUMBER_MESSAGE, Constants.VERIFIED);
        }
        else {
            CommonMethods.showAlertWithTitleAndMessage (context, Constants.VERIFYING_PHONE_NUMBER_MESSAGE, Constants.WRONG_PIN_CODE_MESSAGE );
        }
    }
}
