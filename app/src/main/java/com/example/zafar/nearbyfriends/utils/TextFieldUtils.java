package com.example.zafar.nearbyfriends.utils;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

public class TextFieldUtils extends AppCompatEditText{

    public TextFieldUtils(Context context) {
        super(context);
    }

    public TextFieldUtils(Context context , AttributeSet attrs) {
        super ( context , attrs );
    }

    public boolean isEmpty(){

        return this.getText().toString().equals("");

    }

}
